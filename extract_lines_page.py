# Requires numpy, Pillow

# Looks for all files in source/img, loads them and separates them into lines according to corresponding page in source/page
# outputs to target/img-name_nc.png

import numpy
import os
import sys
import xml.etree.ElementTree as ET

from PIL import Image, ImageDraw


if len(sys.argv) != 3:
    print("First argument: source, second argument: target")
    raise Exception("Ungenügende Argumente")

img_path = os.path.join(sys.argv[1], 'img')
page_path = os.path.join(sys.argv[1], 'page')
out_path = sys.argv[2]
img_files = sorted([f for f in os.scandir(img_path) if f.is_file()], key=lambda f: f.name)

for f in img_files:
    print("Processing {}".format(f.path))
    # We open the image
    # Polygon extraction follows mainly https://stackoverflow.com/a/22650239
    im = Image.open(f.path).convert("RGBA")
    # Let's open the page xml and get all the lines
    tree = ET.parse(os.path.join(page_path, f.name[:-4] + '.xml'))
    root = tree.getroot()
    lines = root.findall(".//*/{*}TextLine")
    lines = root.findall(".//{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}TextLine")
    for l in lines:
        text_id = l.get('id')
        basename = os.path.join(out_path, f.name[:-4] + '_' + text_id)
        coords = l.find('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Coords').get('points')
        polygon = [tuple(map(lambda x: int(x), c.split(','))) for c in coords.split(' ')]
        # We have the coordinates. Wie find a minimal canvas around the polygon.
        # Then we crop this, paste into a new image
        # After that we cut out the polygon, put with transapreny on canvas and then on white canvas
        x_min = min(polygon, key=lambda c: c[0])[0]
        x_max = max(polygon, key=lambda c: c[0])[0]
        y_min = min(polygon, key=lambda c: c[1])[1]
        y_max = max(polygon, key=lambda c: c[1])[1]
        cropIm = Image.new('RGB', (x_max - x_min, y_max - y_min), 0)
        region = im.crop((x_min, y_min, x_max, y_max))
        cropIm.paste(region, (0, 0, x_max - x_min, y_max - y_min))
        crop_polygon = [(t[0] - x_min, t[1] - y_min) for t in polygon]
        # convert to numpy (for convenience)
        imArray = numpy.asarray(cropIm.convert('RGBA'))

        maskIm = Image.new('L', (imArray.shape[1], imArray.shape[0]), 0)
        ImageDraw.Draw(maskIm).polygon(crop_polygon, outline=1, fill=1)
        mask = numpy.array(maskIm)

        # assemble new image (uint8: 0-255)
        newImArray = numpy.empty(imArray.shape,dtype='uint8')

        # colors (three first columns, RGB)
        newImArray[:,:,:3] = imArray[:,:,:3]

        # transparency (4th column)
        newImArray[:,:,3] = mask*200

        # back to Image from numpy
        newIm = Image.fromarray(newImArray, "RGBA")
        finalIm = Image.new("RGBA", newIm.size, 'white')
        finalIm.paste(newIm, (0, 0), newIm)
        finalIm.convert('RGB').save(basename + '.png', 'PNG')

        text = l.find('{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}TextEquiv/{http://schema.primaresearch.org/PAGE/gts/pagecontent/2013-07-15}Unicode').text
        with open(basename + '.gt.txt', 'w') as fout:
            fout.write(text)




