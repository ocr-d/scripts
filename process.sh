#!/bin/bash

# This scripts triggers the process of ocrd and takes some parameters for easier usage
# You pass the following parameters
# -w <path/to/workspace>
# -p <path/to/workflow>
# -l larex-name ;; book name for larex, so that you can see the results

while getopts ":l:p:w:" opt; do
  case $opt in
    l) larex_name="$OPTARG"
      ;;
    p) workflow_path="$OPTARG"
      ;;
    w) workspace_path=$(realpath "$OPTARG")
       echo $workspace_path
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [ -z "$workflow_path" ]
then
  echo "No workflow given"
  exit 1
fi

if [ -z "$workspace_path" ]
then
  echo "No workspace given"
  exit 1
fi

# Get the workflow
echo "Reading workflow"
workflow=$(<${workflow_path})
echo $workflow
echo "Done"

# Currently, docker and ocrd process do have problems with logging, causing some modules to exit
# So we go through our workflow and start docker for each step
#IFS=$'\n'
#for process in $workflow; do
#  process=${process:1} # Takes way leading '
#  process=${process%\'*} # Takes away trailing ' \
#  process="ocrd-$process" # prefix with ocrd-
#  a="time docker run --rm -u $(id -u) -e TESSDATA_PREFIX=/models -v $workspace_path:/data -v ~/models:/models -w /data -- ocrd/all:maximum $process"
#  echo $a
#  eval $a
#  if [ $? != 0 ]
#  then
#    exit $?
#  fi
#
#done


# Run the docker command
echo "Running docker command"
a="/usr/bin/time -o $workspace_path/time -p docker run --rm -u $(id -u) -e TESSDATA_PREFIX=/models -v $workspace_path:/data -v ~/models:/models -w /data -- ocrd/all:maximum ocrd process ${workflow}"
echo $a
eval $a
  if [ $? != 0 ]
  then
    exit $?
  fi
echo "Done"

if [ -z $larex_name ]
then
  echo "LAREX name not set, stopping here"
  exit 1
fi

echo "Copy to larex"

mkdir -p ~/presentation/$larex_name

for file in $workspace_path/OCR-D-IMG/*; do
  echo "Copying file $file"
  mimetype=$(file --mime-type --brief ${file})
  if [[ $mimetype =~ "jpeg" ]]
  then
    cp "$file" "/home/ocrd/presentation/$larex_name/"
  else
    fname=$(basename "$file")
    fname="${fname%.*}.jpg"
    convert "$file" "/home/ocrd/presentation/$larex_name/$fname"
  fi
done

for file in $workspace_path/OCR-D-OCR/*.xml; do
  echo "Copying file $file"
  fname=$(basename "$file")
  fxml="${fname:10:(-8)}.xml"
  cp $file /home/ocrd/presentation/$larex_name/$fxml
done

echo "Done"
