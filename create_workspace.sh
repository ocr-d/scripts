#!/bin/bash

# Creates and populates a new workspace and copies files from source to target

if [ -z "$1" ]
then
  echo "No source given"
  exit 1
fi

if [ -z "$2" ]
then
  echo "No target given"
  exit 1
fi

source_dir=$1
target_dir=$(realpath $2)

# Create new workspace
mkdir -p "$target_dir/OCR-D-IMG"

# Init the workspace
docker run --rm -u $(id -u) -v $target_dir:/data -w /data -- ocrd/all:maximum ocrd workspace init /data

# Create id, currently uuid
uuid=$(uuidgen)
docker run --rm -u $(id -u) -v $target_dir:/data -w /data -- ocrd/all:maximum ocrd workspace set-id $uuid

# Now we can copy and add the files
for filepath in $source_dir/*; do
  mimetype=$(file --mime-type --brief ${filepath})
  if [[ $mimetype =~ "image" ]]
  then
    cp $filepath $target_dir/OCR-D-IMG/
    filename=$(basename -- $filepath)
    echo $filename
    docker run --rm -u $(id -u) -v $target_dir:/data -w /data -- ocrd/all:maximum ocrd workspace add -G OCR-D-IMG -i OCR-D-IMG-${filename} -m ${mimetype} -g img-${filename} OCR-D-IMG/$filename
  fi
done;
